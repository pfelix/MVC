﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class OlaMundoController : Controller
    {
        // GET: OlaMundo
        //public string Index()
        //{
        //    // Este método é chamado por defeito, porque está configurado no ficheiro APP_Start > RouteConfig a rota
        //    // que por defeito chama a action index
        //    return "Esta é a minha Action por <b>defeito</b>";
        //}

        // GET: OlaMundo/Welcome
        //public string Welcome()
        //{
        //    return "Este é o método que a Action Welcome chama...";
        //}

        // GET: OlaMundo/Welcome
        // Os paramentros são passados através de uma query string localhost/olamundo/welcome?nome=rafael&numvezes=10
        // ou seja ...[action]/?[parametro1]=[valeu1]&[parametro2]=[value2]
        // ao meter o ID é uma variável que o sistema já conhece no RouteConfig
        // ou seja ...[action]/[id]?[parametro1]=[valeu1]
        //public string Welcome (string nome, int ID = 1)
        //{
        //    return HttpUtility.HtmlEncode("Olá " + nome + ", Número de vezes: " + ID);
        //}

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome (string nome, int numVezes = 1)
        {
            // Estamos a criar na ViewBag os parametros Mensagem e nome, para conseguirmos utilizar no HTML
            // Na ViewBag somos nós que criamos as propriédades
            ViewBag.Mensagem = "Olá " + nome;
            ViewBag.NumeroVezes = numVezes;
            
            return View();
        }
    }
}
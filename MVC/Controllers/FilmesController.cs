﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class FilmesController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext();

        // GET: Filmes
        public ActionResult Index(string procuraGenero, string procuraFilme)
        {
            var listaGeneros = new List<string>();

            // ir buscar os generos
            var queryGenero = from g in db.Generos orderby g.Descricao select g.Descricao;

            // Adicionar à listaGeneros as descricoes sem repetir dados
            listaGeneros.AddRange(queryGenero.Distinct());

            // Proprieda para carregar os dados na view
            ViewBag.procuraGenero = new SelectList(listaGeneros);

            // Procurar por nome de filme
            var filmes = from f in db.Filmes select f;

            if (!string.IsNullOrEmpty(procuraFilme))
            {
                filmes = filmes.Where(f => f.Titulo.Contains(procuraFilme));
            }

            // Procurar por genero
            var genero = db.Generos.FirstOrDefault(x => x.Descricao == procuraGenero);

            if (!string.IsNullOrEmpty(procuraGenero))
            {
                filmes = filmes.Where(f => f.IdGenero == genero.IdGenero);
            }

            return View(filmes);
        }

        /* // Outra forma utilizando outro metodo neste caso HttpPost
        [HttpPost]
        public string Index(FormCollection fc, string procuraFilme)
        {
            return "<h3>Venho de um [HttpPost] action Index: " + procuraFilme + "</h3>";
        }*/

        // GET: Filmes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == filme.IdGenero);

            if (genero != null)
            {
                // fazemos isto porque no lado da view não conseguimos passar dois model, só um model
                ViewBag.Genero = genero.Descricao;
            }

            return View(filme);
        }

        // GET: Filmes/Create
        public ActionResult Create()
        {
            // Estamos a criar uma lista dos generos ordenada, onde guarda o idGenero, mas mostra a descricção, para mostar numa DropDownList na View
            // Fazemos deste forma, porque na View não conseguimos passar dois Models, só conseguimos passar um Model
            ViewBag.IdGenero = new SelectList(db.Generos.OrderBy(g => g.Descricao), "IdGenero", "Descricao");

            // ViewBag.ListaIdGenero = new SelectList(db.Generos.OrderBy(g => g.Descricao), "IdGenero", "Descricao"); --------------- Perguntar ao rafa questão na View

            return View();
        }

        // POST: Filmes/Create
        [HttpPost]
        public ActionResult Create(Filme novoFilme)
        {

            // Criar um novo ID, na View está escondida ou não aparece
            novoFilme.IdFilme = db.Filmes.Count() + 1;

            if (ModelState.IsValid)
            {
                db.Filmes.InsertOnSubmit(novoFilme);
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Erro = e;
                return View();
            }
        }

        // GET: Filmes/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.IdGenero = new SelectList(db.Generos.OrderBy(g => g.Descricao), "IdGenero", "Descricao");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Filme filme)
        {

            Filme filmeAlterado = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filmeAlterado == null)
            {
                return HttpNotFound();
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == filme.IdGenero);

            if (ModelState.IsValid && genero != null)
            {
                filmeAlterado.Titulo = filme.Titulo;
                filmeAlterado.Data = filme.Data;
                filmeAlterado.IdGenero = genero.IdGenero;
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Filmes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }
            
            return View(filme);
        }

        // POST: Filmes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            db.Filmes.DeleteOnSubmit(filme);

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class GenerosController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext(); // Ligação à BD

        // GET: Generos
        public ActionResult Index()
        {
            return View(db.Generos); // Na prática retorna tudo o que está na tabela Generos para a View index
        }

        // GET: Generos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Generos/Create
        [HttpPost]
        public ActionResult Create(Genero novoGenero)
        {

            if (ModelState.IsValid)
            {
                db.Generos.InsertOnSubmit(novoGenero);
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Generos/Edit/5
        public ActionResult Edit(int? id)  // ? - Torna o parametro opcional, fazendo com que se consiga entrar sempre neste metodo, mesmo quando não se mete o parametro
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            if (genero == null)
            {
                return HttpNotFound();
            }

            return View(genero);
        }

        // POST: Generos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Genero updateGenero)
        {
            Genero generoAlterado = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            if (generoAlterado == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                // atualizamos só a descrição e não o id
                generoAlterado.Descricao = updateGenero.Descricao;
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Generos/Delete/5
        public ActionResult Delete(int? id) // ? - Torna o parametro opcional, fazendo com que se consiga entrar sempre neste metodo, mesmo quando não se mete o parametro
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Genero genero = db.Generos.First(g => g.IdGenero == id); // deste forma está a parar ----------------------------  falar com o Rafa
            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            if (genero == null)
            {
                return HttpNotFound();
            }

            return View(genero);
        }

        // POST: Generos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Genero genero = db.Generos.First(g => g.IdGenero == id);

            if (genero == null)
            {
                return HttpNotFound();
            }

            db.Generos.DeleteOnSubmit(genero);

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
